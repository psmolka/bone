import express from "express";
import graphqlHTTP from "express-graphql";

import schema from "./graphql";

const exampleUser = {
  id: "someID",
  username: "smolka",
  password: "KotNaKlawiaturze",
  email: "pawel.smolka@gmail.com"
};

const root = {
  currentUser: () => exampleUser,
  users() {
    return [exampleUser]
  }
};

const server = express();

server.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
server.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));
