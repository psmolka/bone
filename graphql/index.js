import {
  buildSchema
} from 'graphql';

export default buildSchema(`
  type Query {
    currentUser: User
    users: [User]
  }

  type User {
    id: ID
    username: String,
    pasword: String,
    email: String
  }
`);
